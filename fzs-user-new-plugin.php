<?php
/*
Plugin Name: FZS User New Plugin
Plugin URI: http://the406.com
Description: A simple add-on to support world domination!
Version: 1.0.1
Author: Bradford Knowlton
Author URI: http://www.bradknowlton.com
Text Domain: fzs
Domain Path: /languages

------------------------------------------------------------------------
Copyright 2012-2017 Bradford Knowlton

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
*
* Changelog
* 
* 1.0.1 Initial Version
* 1.0.2 
* 1.0.3 
* 1.0.4 
*
**/


function fzs_load_custom_wp_admin_style($hook) {
        // Load only on ?page=mypluginname
        if($hook != 'user-new.php') {
                return;
        }
        wp_enqueue_style( 'custom_wp_admin_css', plugins_url('css/style.css', __FILE__) );
}
add_action( 'admin_enqueue_scripts', 'fzs_load_custom_wp_admin_style' );


// https://thimpress.com/turn-off-new-user-notification-emails-in-wordpress/

// Fix New User Email Notification
if( !function_exists ( 'wp_new_user_notification')){
	function wp_new_user_notification ( $user_id, $notify = '' ) { }
}

add_filter( 'wpmu_signup_user_notification', '__return_false', 999 ); // Disable confirmation email
add_filter( 'wpmu_welcome_user_notification', '__return_false', 999 ); // Disable welcome email

// https://wordpress.stackexchange.com/questions/23813/adding-fields-to-the-add-new-user-screen-in-the-dashboard

function custom_user_profile_fields(){
	$creating = isset( $_POST['createuser'] );
	$new_user_billing_company = $creating && isset( $_POST['billing_company'] ) ? wp_unslash( $_POST['billing_company'] ) : '';
	
  ?>
    <!-- <h3>Extra profile information</h3> -->
    <table class="form-table">
        <tr class="form-field form-required">
            <th scope="row"><label for="company">Move ID <span class="description"><?php _e('(required)'); ?></span></label></th>
            <td>
                <input type="text" class="regular-text" name="billing_company" value="<?php echo esc_attr( $new_user_billing_company ); ?>" id="billing_company" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60" /><br />
                <!-- <span class="description">Smith-02-2018</span> -->
            </td>
        </tr>
    </table>
  <?php
}
// add_action( 'show_user_profile', 'custom_user_profile_fields' );
// add_action( 'edit_user_profile', 'custom_user_profile_fields' );
add_action( "user_new_form", "custom_user_profile_fields" );

function save_custom_user_profile_fields($user_id){
    # again do this only if you can
    if(!current_user_can('manage_options'))
        return false;

    # save my custom field
    update_user_meta($user_id, 'billing_company', $_POST['billing_company']);
}
add_action('user_register', 'save_custom_user_profile_fields');
// add_action('profile_update', 'save_custom_user_profile_fields');